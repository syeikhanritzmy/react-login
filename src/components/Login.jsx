import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { sigIn } from '../features/user/userSlice';
import { useNavigate } from 'react-router-dom';
export default function Login() {
  const [login, setLogin] = useState();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const handleChange = (e) => {
    const { name, value } = e.target;
    setLogin({ ...login, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const { email, password } = login;
    dispatch(
      sigIn({
        email: email,
        password: password,
        loggedIn: true,
      })
    );
    navigate('../welcome', { replace: true });
  };
  return (
    <div className="h-screen flex justify-center items-center bg-stone-100">
      <form
        action=""
        className="h-3/6 border-2  w-2/6 flex flex-col justify-around items-center"
        onSubmit={handleSubmit}
      >
        <h2 className="w-full text-center pt-2 text-3xl font-bold">SIGN IN</h2>
        <input
          type="email"
          placeholder="Email"
          className="w-8/12 border-2 py-2 px-2 rounded caret-red-300 outline-none  focus:ring-red-300 focus:border-red-300 transition-colors focus:ring-1 "
          name="email"
          onChange={handleChange}
        />
        <input
          type="password"
          placeholder="password"
          className="w-8/12 border-2 py-2 px-2 rounded caret-red-300 outline-none  focus:ring-red-300 focus:border-red-300 transition-colors focus:ring-1"
          name="password"
          onChange={handleChange}
        />

        <button
          className="bg-red-400 px-6 py-3 rounded-sm font-semibold text-white "
          type="submit"
        >
          SUBMIT
        </button>
      </form>
    </div>
  );
}
