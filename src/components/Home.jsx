import React from 'react';
import { useNavigate } from 'react-router-dom';
export default function Home() {
  const navigate = useNavigate();

  return (
    <div className="h-screen flex justify-center items-center">
      <button
        className="bg-blue-400 w-2/12 h-20 shadow-lg rounded-lg font-semibold text-xl text-white"
        onClick={() => {
          navigate('/login');
        }}
      >
        LOGIN
      </button>
    </div>
  );
}
