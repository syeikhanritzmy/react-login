import React from 'react';
import { useDispatch } from 'react-redux';
import { signOut } from '../features/user/userSlice';
import { useNavigate } from 'react-router-dom';
export default function Welcome() {
  const handleLogout = (e) => {
    e.preventDefault();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    dispatch(signOut());
    navigate('../login');
  };
  return (
    <div className="h-screen flex justify-center items-center flex-col">
      <h1 className="font-bold uppercase text-4xl pb-4">
        Welcome <span>CHINEZ 🚀</span>
      </h1>
      <button
        className="px-5 py-3 bg-blue-300 rounded text-white font-semibold shadow text-xl"
        onClick={(e) => handleLogout(e)}
      >
        Logout
      </button>
    </div>
  );
}
