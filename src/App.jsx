import { Route, Routes, useNavigate } from 'react-router-dom';
import Home from './components/Home';
import Login from './components/Login';
import Welcome from './components/Welcome';
import { useSelector } from 'react-redux';
import { selectUser } from './features/user/userSlice';

function App() {
  const user = useSelector(selectUser);

  return (
    <div className="App">
      <Routes>
        <Route element={<Home />} path="/" />
        {user ? (
          <Route element={<Welcome />} path="welcome" />
        ) : (
          <Route element={<Login />} path="login" />
        )}
      </Routes>
    </div>
  );
}

export default App;
