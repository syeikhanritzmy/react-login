/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './index.html',
    './src/**/*.{js,ts,jsx,tsx}',
    './src/components/**/*.{jsx,js}',
  ],
  theme: {
    extend: {},
  },
  plugins: [],
};
